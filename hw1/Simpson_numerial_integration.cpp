#include <iostream>
#include <cmath>

double f(double x){
    return sin(x);
}
double simpon(double a, double b, int n){
    double h = (b - a) / n;
    double sum = f(a) + f(b);
    for (int i = 1; i < n; i++){
        double x = a + i * h;
        if (i % 2 == 0){
            sum += 2 * f(x);
        }
        else{
            sum += 4 * f(x);
        }

    }
    return sum * h / 3;
}
int main(){
    double a = 0;
    double b = M_PI;
    int n = 100;
    double result = simpon(a, b, n);
    std::cout << "The result is " << result << std::endl;
    return 0;
}