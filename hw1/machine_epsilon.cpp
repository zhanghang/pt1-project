#include <iostream>
#include <limits>

int main() {
    std::cout << "Machine epsilon = " << std::numeric_limits<float>::epsilon() << std::endl;
    return 0;
}