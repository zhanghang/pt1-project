#ifndef SIMPSON_HPP
#define SIMPSON_HPP

#include <cmath>
#include <functional>
#include <iostream>

// Forward declarations
struct sin_lambda_x;
template <typename T> struct exp_minus_lambda_x;

template <typename F>
struct domain_t;

template <typename F>
struct result_t;

template <typename F>
typename result_t<F>::type integrate(typename domain_t<F>::type a, typename domain_t<F>::type b, unsigned int bins, F func);

template <typename F>
struct domain_t
{
    using type = typename F::input_t;
};

template <typename F>
struct result_t
{
    using type = typename F::output_t;
};

// Specialization for function pointers
template <typename R, typename T>
struct domain_t<R (*)(T)>
{
    using type = T;
};

template <typename R, typename T>
struct result_t<R (*)(T)>
{
    using type = R;
};

// Specialization for sin_lambda_x
template <>
struct domain_t<sin_lambda_x>
{
    using type = double;
};

template <>
struct result_t<sin_lambda_x>
{
    using type = double;
};

// Specialization for exp_minus_lambda_x
template <typename T>
struct domain_t<exp_minus_lambda_x<T>>
{
    using type = T;
};

template <typename T>
struct result_t<exp_minus_lambda_x<T>>
{
    using type = T;
};

#endif
