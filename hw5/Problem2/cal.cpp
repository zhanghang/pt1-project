#include <iostream>
#include <cmath>
#include <functional>

// 1D Simpson integration function
double integrate_simpson_1d(double a, double b, int num_bins, const std::function<double(double)> &func)
{
    double h = (b - a) / num_bins;
    double result = func(a) + func(b); // endpoints

    for (int i = 1; i < num_bins; i += 2)
    {
        double x_i = a + i * h;
        result += 4 * func(x_i);
    }

    for (int i = 2; i < num_bins - 1; i += 2)
    {
        double x_i = a + i * h;
        result += 2 * func(x_i);
    }

    return result * h / 3.0;
}

// 2D Simpson integration function
double integrate_simpson_2d(double a, double b, double c, double d, int num_bins_x, int num_bins_y, const std::function<double(double, double)> &func)
{
    // Function to integrate over y for a given x
    auto integral_over_y = [&](double x)
    {
        return integrate_simpson_1d(c, d, num_bins_y, [&](double y)
                                    { return func(x, y); });
    };

    // Perform 1D Simpson integration over x
    return integrate_simpson_1d(a, b, num_bins_x, integral_over_y);
}

// Unit disk function
double unit_disk_function(double x, double y)
{
    return (x * x + y * y <= 1.0) ? 1.0 : 0.0;
}

// Density function with exponential decay
double density_function(double x, double y, double R)
{
    return (x * x + y * y <= R * R) ? std::exp(-(x * x + y * y)) : 0.0;
}

int main()
{
    // Test with the unit disk
    double result_unit_disk = integrate_simpson_2d(-1.0, 1.0, -1.0, 1.0, 100, 100, unit_disk_function);
    std::cout << "Integration result for the unit disk: " << result_unit_disk << std::endl;

    // Test with the density function
    double R = 1.7606;
    double result_density_function = integrate_simpson_2d(-R, R, -R, R, 100, 100, [&](double x, double y)
                                                          { return density_function(x, y, R); });
    std::cout << "Integration result for the density function: " << result_density_function << std::endl;

    return 0;
}
