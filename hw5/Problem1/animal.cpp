// animal.cpp

#include "animal.hpp"

namespace Penna
{

    age_t Animal::bad_threshold_ = 0;
    age_t Animal::maturity_age_ = 0;
    double Animal::probability_to_get_pregnant_ = 0.0;

    void Animal::set_maturity_age(age_t R)
    {
        maturity_age_ = R;
    }

    void Animal::set_probability_to_get_pregnant(double b)
    {
        probability_to_get_pregnant_ = b;
    }

    void Animal::set_bad_threshold(age_t T)
    {
        bad_threshold_ = T;
    }

    age_t Animal::get_bad_threshold()
    {
        return bad_threshold_;
    }

    Animal::Animal() : gen_(), age_(0), pregnant_(false) {}

    Animal::Animal(const Genome &genome) : gen_(genome), age_(0), pregnant_(false) {}

    bool Animal::is_dead() const
    {
        return age_ >= maximum_age ;
    }

    bool Animal::is_pregnant() const
    {
        return pregnant_;
    }

    age_t Animal::age() const
    {
        return age_;
    }

    age_t Animal::count_bad_genes() const
    {
        return gen_.count_bad(age_);
    }


    void Animal::grow()
    {
        ++age_;
        pregnant_ = false;
    }

    Animal Animal::give_birth()
    {
        if (age_ >= maturity_age_ && !is_dead() && std::rand() / (RAND_MAX + 1.0) < probability_to_get_pregnant_)
        {
            return Animal(Genome(gen_));
        }
        else
        {
            return Animal();
        }
    }


} // namespace Penna
