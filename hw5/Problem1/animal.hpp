#ifndef ANIMAL_HPP
#define ANIMAL_HPP

#include "genome.hpp"

namespace Penna
{

    class Animal
    {
    public:
        static const age_t maximum_age = Genome::number_of_genes;

        static void set_maturity_age(age_t);
        static void set_probability_to_get_pregnant(double);
        static void set_bad_threshold(age_t);
        static age_t get_bad_threshold();

        Animal();
        Animal(const Genome &);

        bool is_dead() const;
        bool is_pregnant() const;
        age_t age() const;

        void grow();
        Animal give_birth();
        
        age_t count_bad_genes() const; 
    private:
        static age_t bad_threshold_;
        static age_t maturity_age_;
        static double probability_to_get_pregnant_;

        const Genome gen_;
        age_t age_;
        bool pregnant_;
    };

} // namespace Penna

#endif // ANIMAL_HPP
