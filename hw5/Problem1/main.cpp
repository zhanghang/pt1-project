// main.cpp

#include "animal.hpp"
#include <iostream>

int main()
{
    Penna::Animal::set_maturity_age(5);
    Penna::Animal::set_probability_to_get_pregnant(0.5);
    Penna::Animal::set_bad_threshold(3);
    Penna::Genome::set_mutation_rate(2);

    Penna::Animal myAnimal;

    for (int year = 0; year < 10; ++year)
    {
        std::cout << "Year: " << year << ", Age: " << myAnimal.age() << ", Bad Genes: "
                  << myAnimal.count_bad_genes() << ", Pregnant: " << myAnimal.is_pregnant() << std::endl;

        myAnimal.grow();
        Penna::Animal child = myAnimal.give_birth();

        if (!child.is_dead())
        {
            std::cout << "  Child Born! Age: " << child.age() << ", Bad Genes: "
                      << child.count_bad_genes() << std::endl;
        }
    }

    return 0;
}
