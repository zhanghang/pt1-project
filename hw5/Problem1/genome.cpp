#include "genome.hpp"

    namespace Penna
    {

        age_t Genome::mutation_rate_ = 0;

        void Genome::set_mutation_rate(age_t M)
        {
            mutation_rate_ = M;
        }

        age_t Genome::count_bad(age_t n) const
        {
            age_t count = 0;
            for (age_t i = 0; i < n && i < number_of_genes; ++i)
            {
                if (genes_.test(i))
                {
                    ++count;
                }
            }
            return count;
        }

        void Genome::mutate()
        {
            for (age_t i = 0; i < mutation_rate_; ++i)
            {
                age_t index = std::rand() % number_of_genes;
                genes_.flip(index);
            }
        }

    } // namespace Penna
