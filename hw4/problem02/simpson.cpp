#include <iostream>
#include <functional> // for std::function

template <typename Func, typename T>
double simpson(T a, T b, int n, const Func &func)
{
    T h = (b - a) / n;
    T result = func(a) + func(b);

    for (int i = 1; i < n; i += 2)
        result += 4 * func(a + i * h);

    for (int i = 2; i < n - 1; i += 2)
        result += 2 * func(a + i * h);

    return result * h / 3.0;
}

// Function object for exp(−λx)
struct ExpFunction
{
    double lambda;

    ExpFunction(double lambda) : lambda(lambda) {}

    double operator()(double x) const
    {
        return std::exp(-lambda * x);
    }
};

int main()
{
    // Example usage with exp(−λx) function object
    ExpFunction expFunc(1.0); // You can adjust lambda as needed
    double result = simpson(0.0, 1.0, 128, expFunc);

    std::cout << "Simpson integration result: " << result << std::endl;

    return 0;
}
