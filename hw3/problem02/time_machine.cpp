#include <iostream>
#include <limits>

template <typename T>
T computeMachineEpsilon()
{
    T epsilon = 1.;

    while (1. + epsilon != 1.)
        epsilon *= 0.5;

    return 2. * epsilon;
}

int main()
{
    std::cout << "Machine epsilon for float: " << std::numeric_limits<float>::epsilon() << std::endl;
    std::cout << "Computed machine epsilon for float: " << computeMachineEpsilon<float>() << std::endl;

    std::cout << "Machine epsilon for double: " << std::numeric_limits<double>::epsilon() << std::endl;
    std::cout << "Computed machine epsilon for double: " << computeMachineEpsilon<double>() << std::endl;

    std::cout << "Machine epsilon for long double: " << std::numeric_limits<long double>::epsilon() << std::endl;
    std::cout << "Computed machine epsilon for long double: " << computeMachineEpsilon<long double>() << std::endl;

    return 0;
}
