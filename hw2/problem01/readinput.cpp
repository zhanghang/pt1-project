#include<iostream>
#include<fstream>
#include<vector>
#include<array>
std::vector<double> readinput_v(const std::string& filename){
    std::ifstream infile(filename);
    std::vector<double> v;
    double x;
    while (infile >> x){
        v.push_back(x);
    }
    return v;
}
std::array<double, 5> readinput_a(const std::string& filename)
{
    std::ifstream infile(filename);
    std::array<double, 5> arr;
    for (int i = 0; i < 5; i++)
    {
        infile >> arr[i];
    }
    return arr;
}