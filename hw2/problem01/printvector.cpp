#include<vector>
#include<iostream>
#include<array>
std::vector<double> printvector_v(std::vector<double> v){
    for (int i = v.size()-1; i >= 0; i--){
        std::cout << v[i] << std::endl;
    }
    return v;
}
std::array<double, 5> printvector_a(std::array<double, 5> arr){
    for (int i = arr.size()-1; i >= 0; i--){
        std::cout << arr[i] << std::endl;
    }
    return arr;
}