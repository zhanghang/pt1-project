#include <iostream>
#include <vector>
#include <array>
#include "printvector.hpp"
#include "normalise.hpp"
#include "readinput.hpp"
int main(){
    std::vector<double> v = readinput_v("input.txt");
    std::array<double, 5> arr = readinput_a("input.txt");
    std::cout << "Vector before normalisation: " << std::endl;
    printvector_v(v);
    std::cout << "Vector after normalisation: " << std::endl;
    printvector_v(normalise_v(v));
    std::cout << "Array before normalisation: " << std::endl;
    printvector_a(arr);
    std::cout << "Array after normalisation: " << std::endl;
    printvector_a(normalise_a(arr));
    return 0;
  
}
