#include <vector>
#include <array>
#include "normalise.hpp"
std::vector<double> normalise_v(std::vector<double> v){
    double sum = 0;
    for (int i = 0; i < v.size(); i++){
        sum += v[i];
    }
    for (int i = 0; i < v.size(); i++){
        v[i] /= sum;
    }
    return v;
}
std::array<double,5> normalise_a(std::array<double,5> arr){
    double sum = 0;
    for (int i = 0; i < arr.size(); i++){
        sum += arr[i];
    }
    for (int i = 0; i < arr.size(); i++){
        arr[i] /= sum;
    }
    return arr;
}
