#include <iostream>
#include <fstream>
#include <cmath>
#include <numbers>
#include "Simpson.hpp"
int main()
{
    std::ofstream outputFile("result.txt");

    for (int bins = 3; bins <= 10; bins++)
    {
        double result = Simpson(std::sin, 0.0, M_PI, bins);

        // Print result to console (optional)
        std::cout << bins << " " << result << std::endl;

        // Write result to file
        outputFile << bins << " " << result << std::endl;
    }

    return 0;
}
