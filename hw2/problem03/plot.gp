set title "Simpson Integration Results"
set xlabel "Resolution (Number of Bins)"
set ylabel "Integral Value"
plot "result.txt" with linespoints title "Simpson Integration"
