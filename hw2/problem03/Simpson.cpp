#include "Simpson.hpp"
double Simpson(double (*func)(double), double a, double b, int bins)
{
    assert(func != NULL);
    assert(bins > 0);
    double result = 0;
    double dx = (b - a) / bins;
    for (int i = 0; i < bins; i++)
    {
        double x_i = i * dx + a;
        result += dx / 6 * (func(x_i) + 4 * func(x_i + dx / 2) + func(x_i + dx));
    }
    return result;
}