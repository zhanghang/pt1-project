#include <iostream>
#include <cmath>
#include <numbers>
#include <assert.h>
double Simpson(double (*func)(double), double a, double b, int bins);