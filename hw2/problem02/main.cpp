#include "Simpson.hpp"
int main()
{
    double a = 0;
    double b = M_PI;
    int bins = 100;
    double result = Simpson(std::sin, a, b, bins);
    std::cout << "The result is " << result << std::endl;
}
