CC = gcc
AA = ar
CXXFLAGS = -std=c++11

.PHONY: all
all: Simpson

Simpson.o: Simpson.cpp Simpson.hpp
	${CXX} ${CXXFLAGS} -c $<

main.o: main.cpp Simpson.hpp
	${CXX} ${CXXFLAGS} -c $<

libSimpson.a: Simpson.o
	${AA} -crs $@ $<

Simpson: main.o libSimpson.a
	${CXX} ${CXXFLAGS} -o $@ $^ -L. -lSimpson

.PHONY: clean
clean:
	${RM} -v *.o Simpson libSimpson.a
